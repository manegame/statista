const searchRoot = document.querySelector('[data-vue-search]');

/* This is an example Vue app, feel free to use your own code. By default, Vue 2.6 and Axios are available. */

Vue.component('SearchBar', {
    data() {
        return {
            searchTerm: 'statista',
        }
    },
    template: `
        <div
          @keypress.enter="updateResults"
          class="pos-relative searchApp__container"
        >
            <input v-model="searchTerm" placeholder="Search for statistics" type="text" />
            <button
              @click="updateResults"
              type="button"
              class="button button--primary searchApp__submitButton"
            >
                Search
            </button>
        </div>
    `,
    methods: {
      async updateResults () {
        try {
          if (this.searchTerm !== 'statista') {
            this.$emit('update', [])
            return
          }

          const response = await axios.get('https://cdn.statcdn.com/static/application/search_results.json')
          const { items } = response.data

          this.$emit('update', items)
        } catch (error) {
          console.error(error)
          // Handle error on the frontend
        }
      }
    }
});

Vue.component('SearchResults', {
    props: {
      results: {
        type: Array,
        required: true,
        default: () => []
      },
      pageSize: {
        type: Number,
        required: false,
        default: 8
      }
    },
    data () {
      return {
        pageIndex: 0
      }
    },
    template: `
        <section class="searchApp__results">
            <div v-if="results.length === 0" class="panelCard padding-all-20">
              No Results
            </div>
            <template v-else>
              <div class="panelCard padding-all-20 margin-bottom-20">
                Showing {{ paginatedResults.length }} out of {{ results.length }} results
              </div>
              <transition-group appear tag="section" class="width100" name="fade">
                <SearchResult
                  v-for="result in paginatedResults"
                  :key="result.identifier"
                  :result="result"
                />
              </transition-group>  
              <button v-if="paginatedResults.length < results.length" @click="nextPage">
                Load {{ pageSize }} more results
              </button>
            </template>
        </section>
    `,
    computed: {
      paginatedResults () {
        const accumulated = this.pageSize * this.pageIndex
        const end = accumulated + this.pageSize
        
        return this.results.slice(0, end)
      }
    },
    methods: {
      nextPage () {
        this.pageIndex++
      }
    }
});

Vue.component('SearchResult', {
  template: `
    <div class="searchResult box-shadow width100 margin-bottom-30">
      <div class="searchResult__cover" >
        <img loading="lazy" :src="result.teaser_image_urls[0].src">
        <div class="overlay" />
      </div>
      <div class="w100 pos-relative padding-all-20">
        <h4 class="padding-bottom-10 text-semibold">{{ result.title }}</h4>
        <p class="searchResult__description">{{ result.description }}</p>
        <div class="overlay" />
      </div> 
      <div class="w100 padding-all-20 searchResult__actions">
        <div>
          <div v-if="result.premium === 1" class="pill">
            Premium
          </div>
          <div v-else class="pill pill-basic">
            All plans
          </div>
        </div>
        <a :href="result.link" class="linkArrowHover w50">
          <i aria-hidden="true" class="fa fa-long-arrow-right "></i>
          Read more
        </a>
      </div>
    </div>
  `,
  props: {
    result: {
      type: Object,
      required: true
    }
  },
  mounted () {
    console.log(this.result)
  }
})

Vue.component('SearchApp', {
    template: `
        <div>
            <SearchBar @update="handleUpdate" />
            <SearchResults
              :results="results"
            />
        </div>
    `,
    data () {
      return {
        results: []
      }
    },
    methods: {
      handleUpdate (arr) {
        this.results = arr
      }
    }
});

if (searchRoot) {
    new Vue({
        el: searchRoot,
        render(createElement) {
            return createElement('SearchApp');
        }
    });
}

// Final notes

// Ideas for improvement
  // Auto-load more results as user scrolls down (IntersectionObserver)
  // Update results as the user types

  // Also useful if this would work with a state management pattern such as Vuex.
  // I opted for a shortcut here because of two reasons;
    // 1. Time brevity
    // 2. Keeping out extra dependencies
